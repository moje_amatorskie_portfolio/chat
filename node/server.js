var modul = require('./includeIntoServer');//this includes server_local.js or server_remote.js depending if it is local or remote instance
var io = modul.io;

var state = {
    usernames: [],
    rooms: [
        { roomName: 'general'},
        { roomName: '20 something'},
        { roomName: '30 something'},
        { roomName: '40 something'},
        { roomName: '50 something'}        
    ]
}
modul.serve_static();
// app.get('/api/users', (req, res) => {
//   console.log('api/users called');
// });
io.on('connection', (socket) => {
    io.to(socket.id).emit("welcome", state.usernames); //send roomsArray to oneself
    
    //USER JOINED
    socket.on('user joined', (roomName, username) => {
        state.usernames.push({ username: username, socketId: socket.id });
        socket.username = username;
        io.emit('user joined', state.usernames, username);
        console.log('user joined: ' + username);
    });
    //
        
    //DISCONNECT 
    socket.on('disconnect', () => {
        console.log(socket.username + ' disconnected');
        io.emit('disconnection', socket.username + 'disconnected');//to everyone
        removeUsernameFromUsernamesArray(state.usernames, socket.username);
        socket.broadcast.emit('user left', state.usernames, socket.username);
    });
    //
    
    //CHAT MESSAGE  
    socket.on('chat message', (data) => {
        data.socketFromUsername = socket.username; 
        io.emit('chat message', data);  
        console.log('chat message: ' + data.message);
    });
    //
    
    //PRIVATE MESSAGE
    socket.on('private message', (data) => {
        socket.to(data.toSocketId).emit("private message", data);
        console.log('private message: '+ data.message);
    })
    //
    
    //ADD ROOM
    socket.on('add room', (roomName) => {
        console.log('socket.on(add room), roomName:'+roomName);
        let roomObject = {
            roomName: roomName 
        }
        state.rooms.push(roomObject);
        io.emit('room added', state.rooms);
    });
    //
    
    //REFRESH ROOMS
    socket.on('refresh rooms', () => {
        io.emit('refresh rooms', state.rooms);
    })
    //
    
    socket.join('general');
});

modul.server_run();

const removeElementFromArray = function (inputArray, value) {
    const index = inputArray.indexOf(value);
    if( index > -1) {
        inputArray.splice(index, 1);
    }
}

const removeUsernameFromUsernamesArray = (usernamesArray, username) => {
    let filteredUsernamesArray = usernamesArray.filter(usernameObject => usernameObject.username != username);
    state.usernames.splice(0); //if I use usernamesArray then not removing properly, if js array is passed to function by reference, and in node.js it looks like it is not
    state.usernames = [...filteredUsernamesArray]; //ES6 way of cloning arrays
}