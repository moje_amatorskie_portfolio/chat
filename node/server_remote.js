var fs = require('fs');
var app = require('express')();
var http = require('http').createServer(app);
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
const privateKey = fs.readFileSync('/etc/letsencrypt/live/chat.amatorskie-portfolio.online/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/chat.amatorskie-portfolio.online/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/chat.amatorskie-portfolio.online/chain.pem', 'utf8');
const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};
var https = require('https').createServer(credentials, app);
var io = require('socket.io')(https);
const serve_static = () => {
    app.use('/', express.static(path.join(__dirname, '../vue/dist')));
	// app.get('/chat',(req,res)=>res.sendFile(path.join(__dirname, '../vue/dist/index.html')));
}
// http.listen(3080, () => {
//     console.log('listening 3080')
// });
const server_run = () => {
    https.listen(/*443*/3080, () => {
        console.log('https server running on 3080');
    });
}
module.exports = {
    serve_static: serve_static,
    server_run: server_run,
    io: io
}