import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import store from "./store";


new Vue({
  render: h => h(App),
  data: {
      eventBus: new Vue()
  },
  store,
  provide: function () {
      return {
          eventBus: this.eventBus,
      }
  }
}).$mount('#app')
