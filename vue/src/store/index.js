import Vue from "vue";
import Vuex from "vuex";
import { io } from 'socket.io-client';
import findBrowserEngine from '../services/findBrowserEngine';
import refreshStateUsernames from '../services/refreshStateUsernames';
import refreshRoomsService from '../services/refreshRooms';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        messages: [],
        currentRoom: 'general',
        currentPrivateRoom: '',
        isRoomSelected: true, //if false then private message is selected instead
        currentPrivateUsername: '',
        isMessageOutgoing: true,
        toSocketId: '',
        mySocketId: '',
        toUsername: '',
        username: "Donald Duck",
        usernames: [
            {username: 'Janek Kowalski', socketId: ''},
            {username: 'Franek Makowski', socketId: ''}
        ],
        rooms: [
            {roomName: 'general',  messagges: []}, 
            {roomName: '20 something', messagges: []},
            {roomName: '30 something', messagges: []},
            {roomName: '40 something', messagges: []}
        ],
        privateRooms: [],
        roomUsers: [],
        browserEngine: "chrome",
        mutedRooms: [],
        visibleMessageCount: 10,
        showHamburgerChatMessages: true,
        showHamburgerRooms: true,   
        showHamburgerRoomUsers: true,
        stopResizeFromChangingHamburgers: false,
        isRoomUsersHamburgerEnabled: false,
        isRoomsHamburgerDisabled: true
    },
    mutations: {
        sendMessage(currentState, data) {
            if(currentState.isRoomSelected) {
                currentState.socket.emit('chat message', data);
            }   else {
                currentState.socket.emit('private message', data);
            }
        },
        //      //no need to use Vue.set when using push() to array, but need to use it when change one of the elements of the array, because Vue has problems with detecting changes in arrays
        initializeSocket(currentState) {
            currentState.socket = io();
            currentState.mySocketId = currentState.socket.id;
        },
        initializeAudio(currentState) {
            currentState.audio = new Audio('audio.mp3');
            currentState.pawelAudio = new Audio('pawelAudio.mp3');
        },
        playAudio(currentState) {
            currentState.audio.play();
        },
        playPawelAudio(currentState) {
            currentState.pawelAudio.play();
        },
        setBrowserEngine(currentState) {
            let browserEngine = findBrowserEngine();
            currentState.browserEngine = browserEngine;
            console.log('setBrowserEngine: ' + browserEngine);
        },
        setUserJoinedListener(currentState) {
            currentState.socket.on('user joined', (remoteUsersArray, username) => {
                refreshStateUsernames(currentState, remoteUsersArray);
            });
        },
        setUserLeftListener(currentState) {
            currentState.socket.on('user left', (remoteUsersArray, usernameLeft) => {
                refreshStateUsernames(currentState, remoteUsersArray);
                if(currentState.currentRoom == usernameLeft) { //if message window with left user is open, switch to  room 'general'
                    currentState.currentRoom = 'general';
                    currentState.isRoomSelected = true;
                }
            });
        },
        setWelcomeListener(currentState) {
            currentState.socket.on('welcome', (remoteUsersArray) => {
                refreshStateUsernames(currentState, remoteUsersArray);
            })
        },
        getRooms(currentState) {
            currentState.socket.emit('refresh rooms');
        },
        setUsername(currentState, username) {
            currentState.username = username;
        },
        setCurrentRoom(currentState, room) {
            currentState.currentRoom = room;
        },
        setCurrentPrivateRoom(currentState, privateRoom) {
            currentState.currentPrivateRoom = privateRoom;
        },        
        addRoom(currentState, roomName) {
            if(currentState.rooms.length < 10) {
                currentState.socket.emit('add room', roomName);
            }
        },
        addPrivateRoom(currentState, privateRoomObject) {
            currentState.privateRooms.push(privateRoomObject);
        },
        setIsRoomSelected(currentState, value) {
            currentState.isRoomSelected = value;
        },
        setToSocketId(currentState, value) {
            currentState.toSocketId = value;
        },
        setToUsername(currentState, value) {
            currentState.toUsername = value
        },
        setIsMessageOutgoing(currentState, value) {
            currentState.isMessageOutgoing = value;
        },
        setVisibleMessageCount(currentState, count) {
            currentState.visibleMessageCount = count;
        },
        setShowHamburgerRooms(currentState, value) {
            currentState.showHamburgerRooms = value;
        },
        setShowHamburgerChatMessages(currentState, value) {
            currentState.showHamburgerChatMessages = value;
        },
        setShowHamburgerRoomUsers(currentState, value) {
            currentState.showHamburgerRoomUsers = value;
        },
        setStopResizeFromChangingHamburgers(currentState, value) {
            currentState.stopResizeFromChangingHamburgers = value;
        },
        setIsRoomsHamburgerDisabled(currentState, value) {
            currentState.isRoomsHamburgerDisabled = value;
        } 
    }
});