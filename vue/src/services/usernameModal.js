import Swal from 'sweetalert2';
import dataStore from '../store';

export default async function(titleOfModal) { 
    let result = await Swal.fire({
      title: titleOfModal,
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      // showCancelButton: true,
      confirmButtonText: 'OK',
      showLoaderOnConfirm: false,
      allowOutsideClick: () => !Swal.isLoading()
    });
    var isUsernameAlreadyUsed = false;
    var usernames = dataStore.state.usernames;
    for( const usernameObject of usernames) {
        if( result.value == usernameObject.username ) {
            isUsernameAlreadyUsed = true;
        }
    }
    if(result.isConfirmed && result.value != "" && !isUsernameAlreadyUsed) {
        return result.value;
    } else if(isUsernameAlreadyUsed) {
        return result.value + "_" + Math.floor(Math.random() * Math.floor(1000)).toString();
    } else {
        return 'Guest_'+ Math.floor(Math.random() * Math.floor(1000)).toString();
    }
}