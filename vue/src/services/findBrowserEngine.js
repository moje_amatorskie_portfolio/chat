import { detect } from 'detect-browser';
const browser = detect();

export default function findBrowserEngine() {
    return browser.name;    
}