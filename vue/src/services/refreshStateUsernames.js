import dataStore from '../store'; //this is the way to use vuex from outside of components

export default function(currentState, remoteUsersArray) {
        currentState.usernames.splice(0);//it sets usernames length to zero and this will trigger reactive change, similarly like Vue.set
        let filteredRemoteUsersArray = remoteUsersArray.filter(remoteUserObject => remoteUserObject.username != dataStore.state.username);
        currentState.usernames = [...filteredRemoteUsersArray]; //S6 way of cloning arrays
    }
    

