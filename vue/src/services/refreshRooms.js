export default function(currentState, remoteRoomsArrayOfObjects) {
    //         localRoomObject.usernames.splice(0); //it sets usernames length to zero and this will trigger reactive change, similarly like Vue.set
        //let saveUsernames = [...remoteRoomsArrayOfObjects[0].usernames]; //ES6 way of cloning arrays
        currentState.rooms.splice(0); //it sets usernames length to zero and this will trigger reactive change, similarly like Vue.set
        remoteRoomsArrayOfObjects.forEach( roomObject => {
            currentState.rooms.push(roomObject);
        })
    }
    

